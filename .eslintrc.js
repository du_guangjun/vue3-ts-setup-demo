module.exports = {
    root: true,
    env: {
      node: true,
    },
    extends: [
      'eslint:recommended',
      'plugin:vue/vue3-recommended', // 适用于 Vue 3 的 ESLint 规则
      '@vue/typescript/recommended', // 适用于 TypeScript 的 ESLint 规则
    ],
    rules: {
      // 允许 debugger 语句的出现
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    },
  };
  