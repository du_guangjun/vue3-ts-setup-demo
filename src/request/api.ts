import service from ".";
interface LoginData{
    username:string,
    password:string
}
interface GoodsQuery {
  title?: string;
  introduce?: string;
  page?: number;
  pageSize?: number;
}
//获取登录信息
export function login(data:LoginData){
    return service({
        url:"/User",
        method:"get",
        data
    })
}

// 获取商品列表
export function getGoodsList(query: GoodsQuery = {}): Promise<any> {
    return service({
      url: "/GetGoodsList",
      method: "get",
      params: query // 将查询条件和分页信息作为请求的参数
    });
  }
// 获取用户列表
export function getUserList(query: GoodsQuery = {}): Promise<any> {
  return service({
    url: "/GetUserList",
    method: "get",
    params: query // 将查询条件和分页信息作为请求的参数
  });
}
//获取角色列表
export function getRoleList(query: GoodsQuery = {}): Promise<any> {
  return service({
    url: "/GetRoleList",
    method: "get",
    params: query // 将查询条件和分页信息作为请求的参数
  });
}
//获取权限列表接口
export function getAuthorityList(){
  return service({
    url: "/getAuthorityList",
    method: "get"
  });
}