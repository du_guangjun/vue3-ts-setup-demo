import axios from "axios";

const service = axios.create({
  baseURL: "https://localhost:7036", // 移除路径 "/WeatherForecast"
  timeout: 5000,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
  },
});
 
// 请求拦截
service.interceptors.request.use((config) => {
  config.headers = config.headers || {};
  if (localStorage.getItem("token")) {
    config.headers["Authorization"] = `Bearer ${localStorage.getItem("token")}`; // 更新设置请求头的逻辑
  }
  return config;
});

// 响应拦截
service.interceptors.response.use(
  (res) => {
    const code = res.status; // 修改为获取响应的状态码
    if (code !== 200) { // 使用严格不等于 "!=="
      return Promise.reject(res.data);
    }
    return res.data;
  },
  (err) => {
    console.log(err);
    return Promise.reject(err); // 添加返回错误的Promise拒绝
  }
);

export default service;
