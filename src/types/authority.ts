export interface ListInt {
  name: string;
  roleId: number;
  roleList?: ListInt;
  viewRole?: "";
}

export class InitData {
  id: number | null;
  authority: number[] | null;
  constructor(id: number | null, authority: number[] | null) {
    this.id = id;
    this.authority = authority;
  }
  list: ListInt[] = [];
  treeRef:any
}
