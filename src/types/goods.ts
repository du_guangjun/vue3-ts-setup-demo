export interface ListInt {
  userId: number;
  id: number;
  title: string;
  introduce: string;
}
interface selectDataInt {
  title: string;
  introduce: string;
  page: number; //页码
  count: number; //总页数
  pagesize: number; //默认一页显示几条
}
export class InitData {
  selectData: selectDataInt = {
    title: "",
    introduce: "",
    page: 1, //页码
    count: 0, //总页数
    pagesize: 10, //默认一页显示几条
  };
  allList: ListInt[] = [];
  list: ListInt[] = []; //展示的内容的数据
}
