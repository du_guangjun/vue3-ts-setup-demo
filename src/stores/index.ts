import { defineStore } from "pinia";
import { ListInt } from "../types/role";
import { useRouter } from "vue-router";
import { ref } from "vue";

interface RoleState {
  id: number | null;
  authority: number[] | null;
}

// export const useRoleStore = defineStore("roleStore",
//     state: (): RoleState => ({
//       id: null,
//       authority: null,
//     }),
// getters: {//相当于computed就散属性
// }
//     actions: {
//       navigateToAuthorityView(row: ListInt) {
//         this.id = row.roleId;
//         this.authority = row.authority;
//       }
//     })

export const useRoleStore = defineStore("roleStore", {
  state: (): RoleState => {
    //相当于data(){return{}}防止数据污染
    return {
      id: null,
      authority: null,
    };
  },
  // getters: {//相当于computed就散属性
  // }
  actions: {
    //相当于methods
    navigateToAuthorityView(row: ListInt) {
      this.id = row.roleId;
      this.authority = row.authority;
    },
  },
});

// // setup store
// // setupI()[)
// export const useCounterStore = defineStore('main',()=>{
//   const counter=ref(30);//state
//   const gettersCounter=computed(()=>
//   {//getters
//   return counter.value+5;
//   })
//   function addCounter(){//actions
//     counter.value++;
//   }
//   return {counter,gettersCounter,addCounter}
// })
